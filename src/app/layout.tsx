import '@/init';
import type { Metadata } from 'next';
import { cookies } from 'next/headers';
import { dir } from 'i18next';
import GlobalStyles from '@/styled/GlobalStyles';
import Theme from '@/styled/Theme';
import Header from '@/components/Header';
import { fallbackLng } from '@/i18n/settings';

export const metadata: Metadata = {
  title: 'NA Writer'
};

export default function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode;
}>) {
  const cookieStore = cookies();
  const lng = cookieStore.get('lng')?.value;

  return (
    <html lang={lng || fallbackLng} dir={dir(lng || fallbackLng)}>
      <head>
        {/* eslint-disable-next-line @next/next/no-css-tags */}
        <link rel="stylesheet" href="/css/rdg.css" />
      </head>
      <Theme>
        <GlobalStyles />
        <body>
          <div id="root">
            <Header />
            <div id="main">{children}</div>
          </div>
        </body>
      </Theme>
    </html>
  );
}
