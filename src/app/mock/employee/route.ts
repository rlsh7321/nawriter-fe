import { NextRequest, NextResponse } from 'next/server';
import requestData from '@/lib/requestData';

export const POST = async (req: NextRequest) => {
  try {
    const data = await requestData(req);
    console.log(data);
    return NextResponse.json(data);
  } catch (err) {
    console.error(err);
    return NextResponse.json({ msg: `${err}` }, { status: 500 });
  }
};
