'use client';
import React, { useMemo, useState } from 'react';
import styled from '@emotion/styled';
import { SelectColumn, type Column, type SortColumn } from 'react-data-grid';
import orderBy from 'lodash/orderBy';
import StyledDataGrid from '@/components/StyledDataGrid';
import { editCellRenderer } from '@/components/CellRenderers';
import { EmployFilterContext, defaultEmployFilterValue, type EmployFilter } from '@/app/manage/employee/contexts';
import { UuidHeaderCell } from '@/app/manage/employee/CellRenderers';
import useTestData, { type TestData } from '@/app/manage/employee/useTestData';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  overflow-y: hidden;

  .manage-grid-container {
    width: 100%;
    height: 100%;
  }
`;

const columns: Column<TestData>[] = [
  SelectColumn,
  {
    key: 'seq',
    name: 'No',
    width: 200
  },
  {
    key: 'uuid',
    name: 'UUID',
    renderEditCell: editCellRenderer,
    renderHeaderCell: UuidHeaderCell
  }
];

const Employee = () => {
  const [selectedRows, setSelectedRows] = useState<Set<any>>(new Set());
  const [sortColumns, setSortColumns] = useState<SortColumn[]>([]);
  const [filter, setFilter] = useState<EmployFilter>(defaultEmployFilterValue);
  const [pagination, setPagination] = useState({
    page: 0,
    pageSize: 20,
    pageSizeOptions: [5, 10, 20]
  });

  const { total, rows, setRows } = useTestData({
    page: pagination.page,
    pageSize: pagination.pageSize,
    sortColumns,
    filter
  });

  const filteredRows = useMemo(() => rows.filter(({ uuid }) => uuid.includes(filter.uuid)), [rows, filter.uuid]);

  const sortedRows = useMemo(
    () =>
      orderBy(
        filteredRows,
        sortColumns.map(({ columnKey }) => columnKey),
        sortColumns.map(({ direction }) => direction.toLowerCase() as 'asc' | 'desc')
      ),
    [filteredRows, sortColumns]
  );

  const handleSortColumnsChange = (newSortColumns: SortColumn[]) => {
    setSortColumns(newSortColumns);
  };

  return (
    <Container>
      <EmployFilterContext.Provider value={{ ...filter, setFilter }}>
        <StyledDataGrid
          rowKeyGetter={row => row.seq}
          containerClassName="manage-grid-container"
          columns={columns}
          rows={sortedRows}
          selectedRows={selectedRows}
          sortColumns={sortColumns}
          filterEnabled={filter.enabled}
          pagination
          total={total}
          page={pagination.page}
          pageSize={pagination.pageSize}
          pageSizeOptions={pagination.pageSizeOptions}
          onSelectedRowsChange={setSelectedRows}
          onSortColumnsChange={handleSortColumnsChange}
          onRowsChange={setRows}
          onFilterToggleBtnClick={() => setFilter({ ...filter, enabled: !filter.enabled })}
          onFilterResetBtnClick={() => setFilter({ ...filter, ...defaultEmployFilterValue, enabled: filter.enabled })}
          onPageSizeOptionChange={e => setPagination({ ...pagination, page: 0, pageSize: Number(e.target.value) })}
          onPageChange={(e, page) => setPagination({ ...pagination, page: page - 1 })}
        />
      </EmployFilterContext.Provider>
    </Container>
  );
};

export default Employee;
