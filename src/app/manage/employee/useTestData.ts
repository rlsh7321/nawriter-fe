'use client';
import { useEffect, useState } from 'react';
import cloneDeep from 'lodash/cloneDeep';
import orderBy from 'lodash/orderBy';
import { type SortColumn } from 'react-data-grid';
import data from '@/app/manage/data.json';
import { EmployFilter } from '@/app/manage/employee/contexts';

export type TestData = (typeof data)[number];

interface UseTestDataProps {
  page: number;
  pageSize: number;
  sortColumns: SortColumn[];
  filter: EmployFilter;
}

const useTestData = ({ page, pageSize, sortColumns, filter }: UseTestDataProps) => {
  const [total, setTotal] = useState(data.length);
  const [rows, setRows] = useState<TestData[]>([]);

  useEffect(() => {
    const firstIdx = page * pageSize;
    const lastIdx = firstIdx + pageSize;
    const result = orderBy(
      cloneDeep(data).filter(({ uuid }) => uuid.includes(filter.uuid)),
      sortColumns.map(({ columnKey }) => columnKey),
      sortColumns.map(({ direction }) => direction.toLowerCase() as 'asc' | 'desc')
    );
    setTotal(result.length);
    setRows(result.slice(firstIdx, lastIdx));
  }, [page, pageSize, sortColumns, filter]);

  return {
    total,
    rows,
    setRows
  };
};

export default useTestData;
