import React from 'react';

export interface EmployFilter {
  enabled?: boolean;
  uuid: string;
}

export const defaultEmployFilterValue = {
  enabled: false,
  uuid: '',
  setFilter: () => {}
};

export const EmployFilterContext = React.createContext<EmployFilter & { setFilter: (values: EmployFilter) => void }>(
  defaultEmployFilterValue
);
