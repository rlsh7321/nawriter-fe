import React, { useContext } from 'react';
import { TextField } from '@mui/material';
import { type RenderHeaderCellProps } from 'react-data-grid';
import { HeaderCell } from '@/components/CellRenderers';
import { EmployFilterContext } from '@/app/manage/employee/contexts';
import { useTranslation } from '@/i18n/client';

export const UuidHeaderCell = ({ ...props }: RenderHeaderCellProps<any>) => {
  const filter = useContext(EmployFilterContext);
  const { enabled, uuid, setFilter } = filter;
  const { t } = useTranslation();

  return (
    <HeaderCell {...props} filterEnabled={enabled}>
      <TextField
        variant="outlined"
        size="small"
        placeholder={t('components.grid.search')}
        value={uuid}
        style={{ padding: 0 }}
        InputProps={{ style: { margin: 0, fontSize: '14px' } }}
        inputProps={{ style: { padding: '5px', fontSize: '14px' } }}
        onClick={e => e.stopPropagation()}
        onKeyDown={e => e.stopPropagation()}
        onChange={e => setFilter({ ...filter, uuid: e.target.value })}
      />
    </HeaderCell>
  );
};
