'use client';
import { useEffect } from 'react';
import { useRouter } from 'next/navigation';

const Manage = () => {
  const router = useRouter();

  useEffect(() => {
    router.replace('/manage/employee');
  }, [router]);

  return;
};

export default Manage;
