'use client';
import React from 'react';
import styled from '@emotion/styled';
import SideBar from '@/components/SideBar';
import PersonIcon from '@mui/icons-material/Person';
import GroupsIcon from '@mui/icons-material/Groups';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import ApartmentIcon from '@mui/icons-material/Apartment';
import Groups3Icon from '@mui/icons-material/Groups3';
import { useTranslation } from '@/i18n/client';
import { Typography } from '@mui/material';
import { usePathname } from 'next/navigation';

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  gap: 20px;
  overflow: hidden;

  .contents {
    display: flex;
    flex-direction: column;
    width: 100%;
    flex: 1;
    overflow: hidden;
    gap: 20px;

    .contents-title {
      display: flex;
      align-items: center;
      gap: 10px;

      .sidebar-icon {
        width: 32px;
        height: 32px;
      }
    }
  }
`;

const sidebarItems = [
  {
    icon: <PersonIcon className="sidebar-icon" />,
    textKey: 'pages.manage.employee.title',
    href: '/manage/employee'
  },
  {
    icon: <GroupsIcon className="sidebar-icon" />,
    textKey: 'pages.manage.department.title',
    href: '/manage/department'
  },
  {
    icon: <HowToRegIcon className="sidebar-icon" />,
    textKey: 'pages.manage.signup-approval.title',
    href: '/manage/signup-approval'
  },
  {
    icon: <ApartmentIcon className="sidebar-icon" />,
    textKey: 'pages.manage.customer.title',
    href: '/manage/customer'
  },
  {
    icon: <Groups3Icon className="sidebar-icon" />,
    textKey: 'pages.manage.representative.title',
    href: '/manage/representative'
  }
];

const ManageLayout = ({ children }: { children?: React.ReactNode }) => {
  const pathname = usePathname();
  const { t } = useTranslation();

  const items = sidebarItems.map(d => ({
    ...d,
    selected: pathname === d.href || new RegExp(`^${d.href}/`).test(pathname)
  }));
  const selectedItem = items.find(d => d.selected);

  return (
    <Container>
      <div className="sidebar-wrapper">
        <SideBar items={items} />
      </div>
      <div className="contents">
        {selectedItem && (
          <div className="contents-title">
            {selectedItem.icon}
            <Typography variant="h5">{t(selectedItem.textKey)}</Typography>
          </div>
        )}
        {children}
      </div>
    </Container>
  );
};

export default ManageLayout;
