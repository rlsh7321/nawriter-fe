import React from 'react';
import { useTranslation } from '@/i18n/client';
import { IconButton, TextField, Tooltip } from '@mui/material';
import { type RenderEditCellProps, type RenderHeaderCellProps } from 'react-data-grid';
import SaveAsIcon from '@mui/icons-material/SaveAs';
import CloseIcon from '@mui/icons-material/Close';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';

export const HeaderCell = ({
  filterEnabled,
  children,
  ...props
}: RenderHeaderCellProps<any> & { filterEnabled?: boolean; children?: React.ReactNode }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }} tabIndex={props.tabIndex}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          gap: '5px',
          lineHeight: filterEnabled && children ? '40px' : undefined
        }}
      >
        <div>{props.column.name}</div>
        {/asc/i.test(props.sortDirection || '') && <ArrowUpwardIcon style={{ width: '18px', height: '18px' }} />}
        {/desc/i.test(props.sortDirection || '') && <ArrowDownwardIcon style={{ width: '18px', height: '18px' }} />}
      </div>
      {filterEnabled && children}
    </div>
  );
};

const EditCellForm = ({ row, column, onRowChange, onClose }: RenderEditCellProps<any>) => {
  const { t } = useTranslation();

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        e.stopPropagation();
        console.log(e.currentTarget.editor.value);
        onRowChange({ ...row, uuid: e.currentTarget.editor.value }, true);
        onClose(true, false);
      }}
      onKeyDown={e => e.key === 'Enter' && e.stopPropagation()}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        padding: '0 10px',
        gap: '5px'
      }}
    >
      <TextField
        name="editor"
        variant="standard"
        size="small"
        defaultValue={row[column.key as 'seq' | 'uuid']}
        style={{ width: '100%' }}
        InputProps={{ style: { fontSize: 'unset' } }}
        autoFocus
      />
      <Tooltip title={t('components.grid.save')}>
        <IconButton type="submit" size="small" onClick={e => e.stopPropagation()}>
          <SaveAsIcon />
        </IconButton>
      </Tooltip>
      <Tooltip title={t('components.grid.close')}>
        <IconButton type="button" size="small" onClick={() => onClose(false, false)}>
          <CloseIcon />
        </IconButton>
      </Tooltip>
    </form>
  );
};

export function editCellRenderer<T = any>(props: RenderEditCellProps<T>) {
  return <EditCellForm {...props} />;
}
